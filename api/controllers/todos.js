const config = require('../knexfile.js');
const knex = require('knex')(config.development);

const {
  getTodos,
  updateTodo
} = require('../services/todos')

class controller {
  async getTodos(req, res) {
    try {
      const token = await getTodos(knex, req);

      return res.status(200).json(token);
    } catch (e) {
      return res.status(400).json({error: e.message});
    }
  }

  async updateTodo(req, res) {
    try {
      const result = await updateTodo(knex, req);

      return res.status(200).json(result);
    } catch (e) {
      return res.status(400).json({error: e.message});
    }
  }
}

module.exports = new controller();