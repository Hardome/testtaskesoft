const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const login = async (knex, req) => {
  const {login, password} = req.body;

  const [user] = await knex('users')
    .where('login', login)
    .select('id', 'password');

  if(!user){
    throw Error('Пользователь с таким логином не найден');
  }

  const validPassword = bcrypt.compareSync(password, user.password);

  if(!validPassword){
    throw Error('Пароль неверный');
  }

  const [userData] = await knex('users')
    .where('id', user.id)
    .select('firstName', 'secondName', 'surName', 'id', 'idLeader');

  return {token: createToken(user.id, userData), userData}
}

const createToken = (id, data) => {
  let payload = {
    id,
    firstName: data.firstName,
    secondName: data.secondName,
    surName: data.surName,
    idLeader: data.idLeader
  }
  return jwt.sign(payload, 'SecretKey');
}

module.exports = {
  login
};