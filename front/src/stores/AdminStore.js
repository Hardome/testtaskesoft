import {makeAutoObservable} from 'mobx';

class AdminStore {
  constructor(userStore) {
    makeAutoObservable(this, {}, {autoBind: true});
    this.userStore = userStore;
    this._init();
  }

  leadersOptions = [];
  user = {};

  _init = async () => {
    await this.getUsers();
  }

  setUserProp = (key, value) => {
    this.user[key] = value;
  }

  setLeaders = (users) => {
    this.leadersOptions = users.map((user) => {
      return {value: user.id, label: `${user.surName} ${user.firstName[0]}. ${user.secondName[0]}.`}
    })
  }

  addUser = async () => {
    if (!this.user.firstName || !this.user.secondName || !this.user.surName || !this.user.login || !this.user.password) {
      return alert('Заполните поля!');
    }

    const res = await fetch(`http://localhost:8000/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(this.user)
    });
    const result = await res.json();

    if (res.ok) {
      await this.getUsers();
    }

    alert(result.message);
  }

  getUsers = async () => {
    const res = await fetch(`http://localhost:8000/users`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    });
    const users = await res.json();

    if (res.ok) {
      this.setLeaders(users);
    }
  }
}

export default AdminStore;