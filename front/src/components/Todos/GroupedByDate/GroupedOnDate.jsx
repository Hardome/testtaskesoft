import React from 'react';
import s from './GroupedOnDate.module.scss'
import Text from '../../shared/Text/Text';
import Todo from '../components/Todo';
import {inject, observer} from 'mobx-react';

const GroupedOnDate = (props) => {
  const store = props.todoStore;
  const modalStore = props.modalStore;
  const {groupedTodos} = store;
  const {setModalIsOpen, setCurrentTodo} = modalStore;
  const openTodo = (todo) => {
    setCurrentTodo(todo);
    setModalIsOpen(true);
  }

  return (
    <div>
      {
        groupedTodos.todayTodos.length > 0 &&
        <div className={s.group}>
          <Text className={s.headerRow} text={'На сегодня'} variant={'font24'}></Text>
          <div className={s.todos}>
            {
              groupedTodos.todayTodos?.map((todo, i) => {
                return <Todo
                  key={i}
                  header={todo.header}
                  priority={todo.priority.label}
                  dateEnd={todo.dateEnd}
                  responsible={todo.responsible.label}
                  status={todo.status}
                  onClick={() => openTodo(todo)}
                />
              })
            }
          </div>
        </div>
      }
      {
        groupedTodos.weekTodos.length > 0 &&
        <div className={s.group}>
          <Text className={s.headerRow} text={'На неделю'} variant={'font24'}></Text>
          <div className={s.todos}>
            {
              groupedTodos.weekTodos?.map((todo, i) => {
                return <Todo
                  key={i}
                  header={todo.header}
                  priority={todo.priority.label}
                  dateEnd={todo.dateEnd}
                  responsible={todo.responsible.label}
                  status={todo.status}
                  onClick={() => openTodo(todo)}
                />
              })
            }
          </div>
        </div>
      }
      {
        groupedTodos.futureTodos.length > 0 &&
        <div className={s.group}>
          <Text className={s.headerRow} text={'На будущее'} variant={'font24'}></Text>
          <div className={s.todos}>
            {
              groupedTodos.futureTodos?.map((todo, i) => {
                return <Todo
                  key={i}
                  header={todo.header}
                  priority={todo.priority.label}
                  dateEnd={todo.dateEnd}
                  responsible={todo.responsible.label}
                  status={todo.status}
                  onClick={() => openTodo(todo)}
                />
              })
            }
          </div>
        </div>
      }
    </div>
  )
};
export default inject('todoStore')(observer(GroupedOnDate));