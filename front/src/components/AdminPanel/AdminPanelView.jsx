import React from 'react';
import s from './AdminPanel.module.scss'
import {observer} from 'mobx-react';
import Text from '../shared/Text/Text';
import Input from "../shared/Input/Input";
import Select from '../shared/Select/Select'
import Button from "../shared/Button/Button";

const AdminPanelView = (props) => {
  const store = props.adminStore;
  const {
    user,
    leadersOptions,
    setUserProp,
    addUser
  } = store;

  return (
    <div className={s.container}>
      <div className={s.form}>
        <Text variant={'font24'} text={'Добавить пользователя'}/>
        <Input
          placeholder={'Логин'}
          value={user?.login || ''}
          onChange={(e) => setUserProp('login', e.target.value)}
          disabled={false}
        />
        <Input
          placeholder={'Пароль'}
          value={user?.password || ''}
          onChange={(e) => setUserProp('password', e.target.value)}
          disabled={false}
        />
        <Input
          placeholder={'Фамилия'}
          value={user?.surName || ''}
          onChange={(e) => setUserProp('surName', e.target.value)}
          disabled={false}
        />
        <Input
          placeholder={'Имя'}
          value={user?.firstName || ''}
          onChange={(e) => setUserProp('firstName', e.target.value)}
          disabled={false}
        />
        <Input
          placeholder={'Отчество'}
          value={user?.secondName || ''}
          onChange={(e) => setUserProp('secondName', e.target.value)}
          disabled={false}
        />
        <div>
        <Select
          options={leadersOptions}
          onChange={setUserProp}
          value={user?.idLeader?.value}
          type={'idLeader'}
        />
          <Text text={'Селект руководителя (при создании руководителя оставить пустым)'}/>
        </div>
        <Button
          text={'Сохранить'}
          onClick={addUser}
        />
      </div>
    </div>
  )
};

export default (observer(AdminPanelView));