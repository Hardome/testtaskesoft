import {makeAutoObservable} from 'mobx';
import {loadingStatus} from '../consts';

class UserStore {
  constructor() {
    makeAutoObservable(this, null, {autoBind: true});
    this._init();
  }

  loginValue = '';
  passwordValue = '';
  incorrectLogin = false;
  incorrectPassword = false;
  loadingStatus = loadingStatus.SUCCESS;
  userData = {};
  isLeader = false;

  changeLogin = (value) => {
    this.loginValue = value;
  }

  changePassword = (value) => {
    this.passwordValue = value;
  }

  setIncorrectLogin = (value) => {
    this.incorrectLogin = value;
  }

  setIncorrectPassword = (value) => {
    this.incorrectPassword = value;
  }

  setUserData = (userData) => {
    this.userData = userData;
    this.setUserIsLeader(userData.idLeader);
  }

  setUserIsLeader = (id) => {
    this.isLeader = !id;
  }

  clearErrors = () => {
    this.setIncorrectPassword(false);
    this.setIncorrectLogin(false);
  }

  logout = () => {
    localStorage.removeItem('token');
    this.userData = null;
  }

  setLoadingStatus = (value) => {
    this.loadingStatus = value;
  }

  auth = async () => {
    this.clearErrors();
    this.setLoadingStatus(loadingStatus.LOADING);

    const res = await fetch('http://localhost:8000/auth', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        login: this.loginValue,
        password: this.passwordValue
      })
    });
    await new Promise(resolve => setTimeout(resolve, 300));
    const {token, userData, error} = await res.json();

    if (res.ok) {
      this.clearErrors();
      this.setUserData(userData);
      localStorage.setItem('token', token);
      this.setLoadingStatus(loadingStatus.SUCCESS);
    } else {
      if (error === 'Пароль неверный') {
        this.setIncorrectPassword(true);
      } else if (error === 'Пользователь с таким логином не найден') {
        this.setIncorrectLogin(true);
      }
      this.setLoadingStatus(loadingStatus.SUCCESS);
    }
  }

  _init = async () => {
    const token = localStorage.getItem('token');
    this.setLoadingStatus(loadingStatus.LOADING);
    try {
      await new Promise(resolve => setTimeout(resolve, 300));
      const res = await fetch('http://localhost:8000/auth/check', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({token})
      });
      const userData = await res.json();
      this.setUserData(userData);
      this.setLoadingStatus(loadingStatus.SUCCESS);
      this.clearErrors();
    } catch (e) {
      localStorage.removeItem(token);
      this.setLoadingStatus(loadingStatus.SUCCESS);
    }
  }
}

export default UserStore;