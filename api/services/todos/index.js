const {getTodos} = require('./getTodos');
const {updateTodo} = require('./updateTodo');

module.exports = {
  getTodos,
  updateTodo
};