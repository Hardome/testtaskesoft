/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  await knex.schema.createTable('todos', (table) => {
    table.increments('id');
    table.string('header').notNullable();
    table.text('description').notNullable();
    table.timestamp('dateEnd').notNullable();
    table.timestamp('createdAt').notNullable();
    table.timestamp('updatedAt');
    table.integer('priority').notNullable();
    table.integer('status').notNullable();
    table.integer('idAuthor');
    table.foreign('idAuthor')
      .references('id')
      .inTable('users');
    table.integer('idResponsible');
    table.foreign('idResponsible')
      .references('id')
      .inTable('users');
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  await knex.schema.dropTable('todos');
};
