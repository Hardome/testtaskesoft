import React from 'react';
import s from './Text.module.scss'
import cn from "classnames";
const Text = (props) => {
  const {variant} = props;
  let variantClassName, colorClassName;

  if(props.color === 'White'){
    colorClassName = s.white;
  } else {
    colorClassName = s.black;
  }

  switch (variant) {
    case 'font16':
      variantClassName = s.font16;
      break;
    case 'font24':
      variantClassName = s.font24;
      break;
    default:
      variantClassName = s.base;
  }

  return <div className={cn(s.text, variantClassName, props.className, colorClassName)}>{props.text}</div>
};

export default Text;