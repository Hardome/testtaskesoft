const getTodos = async (knex, req) => {
  const id = req.query.id;

  const todos = await knex('todos')
    .where('idResponsible', id)
    .orWhere('idAuthor', id)
    .leftJoin('users as userRes', 'userRes.id', '=', 'todos.idResponsible')
    .leftJoin('users as author', 'author.id', '=', 'todos.idAuthor')
    .select('todos.id', 'todos.status', 'todos.idResponsible', 'todos.idAuthor',
      'todos.createdAt', 'todos.updatedAt', 'todos.priority', 'todos.header',
      'todos.description', 'todos.dateEnd',
      'userRes.id as resId', 'userRes.firstName as resFirstName', 'userRes.surName as resSurName',
    'userRes.secondName as resSecondName',
      'author.id as authorId', 'author.firstName as authorFirstName', 'author.surName as authorSurName',
      'author.secondName as authorSecondName',);

  return todos;
}

module.exports = {
  getTodos
};