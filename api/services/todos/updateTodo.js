const updateTodo = async (knex, req) => {
  const todo = req.body;
  const dateEnd = todo.dateEnd ? new Date(todo.dateEnd) : new Date(Date.now());

  if (todo.id) {
    await knex('todos')
      .where('id', todo.id)
      .update({
        idResponsible: todo.responsible,
        dateEnd: dateEnd,
        updatedAt: new Date(Date.now()),
        priority: todo.priority,
        status: todo.status,
        header: todo.header,
        description: todo.description
      });
  } else {
    await knex('todos')
      .insert({
        idResponsible: todo.responsible,
        dateEnd: dateEnd,
        updatedAt: new Date(Date.now()),
        createdAt: new Date(Date.now()),
        priority: todo.priority,
        status: todo.status,
        header: todo.header,
        description: todo.description,
        idAuthor: todo.idAuthor
      });
  }

  return 'Ok';
}

module.exports = {
  updateTodo
};