const getUsers = async (knex, req) => {
  const id = req.query.id;
  let users;

  if (id) {
    users = await knex('users')
      .where('idLeader', id)
      .orWhere('id', id)
      .select('id', 'firstName', 'secondName', 'surName');
  } else {
    users = await knex('users')
      .where('idLeader', null)
      .select('id', 'firstName', 'secondName', 'surName');
  }

  return users;
}

module.exports = {
  getUsers
};