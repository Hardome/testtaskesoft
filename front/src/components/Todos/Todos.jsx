import {inject, observer} from 'mobx-react';
import {Provider} from '../../utils/provider';
import TodosView from './TodosView';
import TodoStore from '../../stores/TodoStore';
import ModalStore from "../../stores/ModalStore";

const Todos = (props) => {
  const userStore = props.userStore;
  const todoStore = new TodoStore(userStore);
  const modalStore = new ModalStore(userStore, todoStore);

  return (
    <Provider todoStore={todoStore}>
      <TodosView todoStore={todoStore} modalStore={modalStore} />
    </Provider>
  )
}
export default inject('userStore')(observer(Todos));