import React from 'react';
import s from './WithoutGrouping.module.scss'
import Todo from '../components/Todo';
import {inject, observer} from 'mobx-react';

const WithoutGrouping = (props) => {
  const store = props.todoStore;
  const modalStore = props.modalStore;
  const {
    groupedTodos
  } = store;
  const {
    setModalIsOpen,
    setCurrentTodo
  } = modalStore;

  const openTodo = (todo) => {
    setCurrentTodo(todo);
    setModalIsOpen(true);
  }

  return (
    <div>
      { groupedTodos.withoutGrouping?.length > 0 &&
      <div className={s.group}>
        {
          groupedTodos.withoutGrouping?.map((todo, i) => {
            return <Todo
              key={i}
              header={todo.header}
              priority={todo.priority.label}
              dateEnd={todo.dateEnd}
              responsible={todo.responsible.label}
              status={todo.status}
              onClick={() => openTodo(todo)}
            />
          })
        }
      </div>
      }
    </div>
  )
};
export default inject('todoStore')(observer(WithoutGrouping));