const jwt = require('jsonwebtoken');
const check = (knex, req) => {
  const {token} = req.body;
  const userData = jwt.verify(token, 'SecretKey');

  if (!userData) {
    throw Error('Невалидный токен');
  }

  return userData;
}

module.exports = {
  check
};