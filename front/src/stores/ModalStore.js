import {makeAutoObservable, toJS} from 'mobx';
import {loadingStatus, todoPriority, todoStatus} from '../consts';

class ModalStore {
  constructor(userStore, todoStore) {
    makeAutoObservable(this, {}, {autoBind: true});
    this.userStore = userStore;
    this.refreshTodos = todoStore.refreshTodos;
    this._init();
  }

  _init = async () => {
    await this.getMyResponsibilities();
  }

  modalIsOpen = false;
  currentTodo = {};
  responsibilitiesOptions = [];
  loadingStatus = loadingStatus.SUCCESS;
  priorityOptions = [
    {label: 'Высокий', value: todoPriority.HIGH},
    {label: 'Средний', value: todoPriority.MEDIUM},
    {label: 'Низкий', value: todoPriority.LOW}
  ]
  statusOptions = [
    {label: 'Отменена', value: todoStatus.CANCELED},
    {label: 'Выполнена', value: todoStatus.COMPLETED},
    {label: 'Выполняется', value: todoStatus.IN_PROGRESS},
    {label: 'К выполнению', value: todoStatus.TO_EXECUTE}
  ]

  setCurrentTodoProp = (key, value) => {
    this.currentTodo[key] = value;
  }

  setLoadingStatus = (value) => {
    this.loadingStatus = value;
  }

  setCurrentTodo = (todo) => {
    this.currentTodo = toJS(todo);
  }
  setModalIsOpen = (value) => {
    this.modalIsOpen = value;
  }

  setTodoChanges = async () => {
    if (!this.currentTodo.priority?.value || !this.currentTodo.status?.value || !this.currentTodo.header
      || !this.currentTodo.responsible?.value || !this.currentTodo.description) {
      return alert('Заполните поля!');
    }

    try {
      this.setLoadingStatus(loadingStatus.LOADING);

      const {priority, status, responsible, dateEnd, ...rest} = toJS(this.currentTodo);
      const todoToSent = {
        ...rest,
        priority: priority.value,
        status: status.value,
        responsible: responsible.value,
        dateEnd: new Date(dateEnd),
        idAuthor: this.userStore.userData.id
      };

      const res = await fetch(`http://localhost:8000/todos`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(todoToSent)
      });
      await new Promise(resolve => setTimeout(resolve, 300));

      if (res.ok) {
        await this.refreshTodos();
        this.setLoadingStatus(loadingStatus.SUCCESS);
      }

    } catch (e) {
      alert('Ошибка!')
      this.setLoadingStatus(loadingStatus.SUCCESS);
    }
  }

  setMyResponsibilities = (responsibilities) => {
    if (responsibilities.length) {
      this.responsibilitiesOptions = responsibilities.map(user => ({
        value: user.id,
        label: `${user.surName} ${user.firstName[0]}. ${user.secondName[0]}.`
      }));
    } else {
      this.responsibilitiesOptions = [{
        value: this.userStore.userData.id,
        label: `${this.userStore.userData.surName} ${this.userStore.userData.firstName[0]}. ${this.userStore.userData.secondName[0]}.`
      }]
    }
  }

  getMyResponsibilities = async () => {
    const res = await fetch(`http://localhost:8000/users?id=${this.userStore.userData.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    });

    const responsibilities = await res.json();

    if (res.ok) this.setMyResponsibilities(responsibilities);
  }
}

export default ModalStore;