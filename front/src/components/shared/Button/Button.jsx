import React from 'react';
import s from './Button.module.scss'
import {observer} from "mobx-react";
import cn from "classnames";

const Button = (props) => {

  return (
    <button
      className={cn(s.button, props.className)}
      onClick={props.onClick}>{props.text}
    </button>
  )
};

export default observer(Button);