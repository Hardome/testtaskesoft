import React from 'react';
import s from './Input.module.scss'
import {observer} from 'mobx-react';
import cn from 'classnames';

const Input = (props) => {

  return (
    <input
      disabled={props.disabled}
      className={cn(s.input, props.className)}
      onChange={props.onChange}
      value={props.value}
      type={props.type}
      placeholder={props.placeholder}>
    </input>
  )
};

export default observer(Input);