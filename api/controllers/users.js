const config = require('../knexfile.js');
const knex = require('knex')(config.development);

const {
  getUsers,
  addUser
} = require('../services/users')

class controller {
  async getUsers(req, res) {
    try {
      const token = await getUsers(knex, req);

      return res.status(200).json(token);
    } catch (e) {
      return res.status(400).json({error: e.message});
    }
  }

  async addUser(req, res) {
    try {
      const result = await addUser(knex, req);

      return res.status(200).json(result);
    } catch (e) {
      return res.status(400).json({error: e.message});
    }
  }
}

module.exports = new controller();