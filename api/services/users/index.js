const {getUsers} = require('./getUsers');
const {addUser} = require('./addUser');

module.exports = {
  getUsers,
  addUser
};