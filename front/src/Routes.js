import React from 'react';
import {
  createBrowserRouter,
  RouterProvider
} from 'react-router-dom';
import Auth from './components/Authorization/Auth';
import WithHeader from './components/WithHeader/WithHeader';
import Todo from './components/Todos/Todos';
import {observer} from 'mobx-react';
import AdminPanel from './components/AdminPanel/AdminPanel';

const router = createBrowserRouter([
  {
    path: '*',
    element: <WithHeader component={<Todo /> } />
  },
  {
    path: '/admin-panel',
    element: <WithHeader component={<AdminPanel /> } />
  }
]);

const RouterComponent = (props) => {
  if(!props.userStore.userData?.id){
    return <Auth />
  }

  return <RouterProvider router={router} />;
}

export default observer(RouterComponent);