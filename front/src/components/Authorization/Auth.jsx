import React from 'react';
import s from './Auth.module.scss'
import Input from '../shared/Input/Input';
import {inject, observer} from 'mobx-react';
import Button from '../shared/Button/Button';
import Text from '../shared/Text/Text';
import {CircularProgress} from '@mui/material';
import {loadingStatus as loadingStatusEnum} from '../../consts';

const Authorization = (props) => {
  const store = props.userStore;
  const {
    loginValue,
    incorrectLogin,
    changeLogin,
    passwordValue,
    incorrectPassword,
    changePassword,
    auth,
    loadingStatus
  } = store;

  return (
    <div className={s.body}>
      <div className={s.form}>
        <Text
          className={s.text}
          variant={'font24'}
          text={'Авторизация'}
          color={'Black'}
        />
        {loadingStatus === loadingStatusEnum.LOADING && <CircularProgress/>}
        {
          !localStorage.getItem('token') &&
          <div>
            <div className={s.input}>
              <Input
                text={'Логин'}
                onChange={(e) => changeLogin(e.target.value)}
                value={loginValue}
                isCorrect={incorrectLogin}
              />
              <span
                className={s.span}>{incorrectLogin ? 'Пользователя с таким логином не существует' : ''}
            </span>
            </div>
            <div className={s.input}>
              <Input
                text={'Пароль'}
                onChange={(e) => changePassword(e.target.value)}
                value={passwordValue}
                isCorrect={incorrectPassword}
                type={'password'}
              />
              <span
                className={s.span}>{incorrectPassword ? 'Неверный пароль' : ''}
            </span>
            </div>
            <Button
              onClick={auth}
              text={'Вход'}
            />
          </div>}
      </div>
    </div>
  )
};

export default inject('userStore')(observer(Authorization));