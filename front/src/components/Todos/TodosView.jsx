import React from 'react';
import {inject, observer} from 'mobx-react';
import s from './Todos.module.scss';
import GroupedOnDate from './GroupedByDate/GroupedOnDate';
import Button from '../shared/Button/Button';
import WithoutGrouping from './WithoutGrouping/WithoutGrouping';
import GroupedByResponsible from './GroupedByResponsible/GroupedByResponsible';
import Modal from '../Modal/Modal';
import {groupingType} from "../../consts";

const TodosView = (props) => {
  const store = props.todoStore;
  const modalStore = props.modalStore;
  const userStore = props.userStore;
  const isLeader = userStore.isLeader;
  const {modalIsOpen, setModalIsOpen} = modalStore;
  const {
    selectedGroupingType,
    setSelectedGroupingType,
    refreshTodos
  } = store;

  return (
    <div className={s.container}>
      <div className={s.buttons}>
        <Button text={'Без группировки (сортировка по обновлению)'}
                className={s.button}
                onClick={() => setSelectedGroupingType(groupingType.WITHOUT_GROUPING)}
        />
        <Button text={'C группировкой по дате'}
                className={s.button}
                onClick={() => setSelectedGroupingType(groupingType.WITH_GROUPING_BY_DATE)}
        />
        {isLeader &&
          <Button text={'C группировкой по ответственным'}
                  className={s.button}
                  onClick={() => setSelectedGroupingType(groupingType.WITH_GROUPING_BY_RESPONSIBLE)}
          />}
        <Button text={'Обновить'} className={s.button} onClick={refreshTodos} />
        {isLeader && <Button text={'Создать задачу'} className={s.button} onClick={() => setModalIsOpen(true)}/>}
      </div>
      <div className={s.form}>
        {selectedGroupingType === groupingType.WITHOUT_GROUPING && (<WithoutGrouping modalStore={modalStore} />)}
        {selectedGroupingType === groupingType.WITH_GROUPING_BY_DATE && (<GroupedOnDate modalStore={modalStore} />)}
        {selectedGroupingType === groupingType.WITH_GROUPING_BY_RESPONSIBLE && (<GroupedByResponsible modalStore={modalStore} />)}
      </div>
      <Modal isOpen={modalIsOpen} onClose={() => setModalIsOpen(false)} modalStore={modalStore} />
    </div>
  )
};

export default inject('userStore')(observer(TodosView));