import Routes from './Routes';
import UserStore from './stores/UserStore';
import {Provider} from "./utils/provider";

function App() {
  const userStore = new UserStore();

  return (
    <Provider userStore={userStore}>
      <Routes userStore={userStore} />
    </Provider>
  );
}

export default App;
