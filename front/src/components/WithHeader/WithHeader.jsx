import React from 'react';
import Header from "../Header/Header";
import s from './WithHeader.module.scss'

const WithHeader = (props) => {

  return (
    <div className={s.page}>
      <Header store={props.store} />
      {props.component}
    </div>
  )};
export default WithHeader;