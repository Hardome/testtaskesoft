const config = require('../knexfile.js');
const knex = require('knex')(config.development);

const {
  login,
  check
} = require('../services/auth')

class controller {
  async login(req, res) {
    try {
      const token = await login(knex, req);

      return res.status(200).json(token);
    } catch (e) {
      return res.status(400).json({error: e.message});
    }
  }

  check(req, res) {
    try {
      const userData = check(knex, req);

      return res.status(200).json(userData);
    } catch (e) {
      return res.sendStatus(401);
    }
  }
}

module.exports = new controller();