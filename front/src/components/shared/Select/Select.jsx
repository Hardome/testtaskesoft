import React from 'react';
import {observer} from "mobx-react";
import {MenuItem} from "@mui/material";
import {Select} from '@mui/material';

const SelectShared = (props) => {

  return (
    <Select
      disabled={props.disabled}
      variant={'standard'}
      size={props.size}
      multiple={props.isMulti}
      onChange={(ev) => props.onChange(props.type, {value: ev.target.value})}
      sx={{width: '100%'}}
      value={props.value || ''}
      label={props.label}
      className={props.className}
    >
      {
        props.options?.map((el, i) =>
          <MenuItem key={i} value={el.value}>{el.label}</MenuItem>)
      }
    </Select>
  );
}

export default (observer(SelectShared));
