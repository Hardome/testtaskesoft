import React from 'react';
import {inject, observer} from 'mobx-react';
import AdminPanelView from './AdminPanelView';
import AdminStore from "../../stores/AdminStore";

const AdminPanel = (props) => {
  const userStore = props.userStore;
  const adminStore = new AdminStore(userStore);

  if (!userStore.isLeader) {
    window.location.pathname = '/todos';
  }

  return (
    <AdminPanelView adminStore={adminStore} />
  )
};

export default inject('userStore')(observer(AdminPanel));