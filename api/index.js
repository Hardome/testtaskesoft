const express = require('express');
const cors = require('cors');
const authRouter = require('./routes/auth');
const todosRouter = require('./routes/todos');
const usersRouter = require('./routes/users');
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/auth', authRouter);
app.use('/todos', todosRouter);
app.use('/users', usersRouter);

app.listen(8000, () => {
  console.log(`Server started on port ${8000}`);
});

module.exports = app;