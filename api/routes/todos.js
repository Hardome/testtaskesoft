const express = require('express');
const router = express.Router();
const controller = require('../controllers/todos');

router.get('/', controller.getTodos)
router.post('/', controller.updateTodo)

module.exports = router;