const {hashSync} = require('bcrypt');
const addUser = async (knex, req) => {
  const {login, password, idLeader, surName, firstName, secondName} = req.body;

  const [candidate] = await knex('users').where('login', login)
    .select('id', 'password');

  if (candidate) {
    return {message: 'Такой пользователь существует'};
  }
  const hashPassword = hashSync(password, 7);

  await knex('users').insert({
    login: login,
    password: hashPassword,
    idLeader: idLeader ? idLeader.value : null,
    surName: surName,
    firstName: firstName,
    secondName: secondName
  });

  return {message: 'Пользователь зарегестрирован'};
}

module.exports = {
  addUser
};