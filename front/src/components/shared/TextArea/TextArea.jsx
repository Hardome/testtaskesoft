import React from 'react';
import s from './TextArea.module.scss'
import {observer} from "mobx-react";

const Input = (props) => {

  return (
    <textarea
      className={s.textArea}
      placeholder="Описание задачи"
      onChange={props.onChange}
      disabled={props.disabled}
      value={props.value}
    />
  )
};

export default observer(Input);