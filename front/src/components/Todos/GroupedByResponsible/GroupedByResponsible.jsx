import React from 'react';
import s from './GroupedByResponsible.module.scss'
import Text from '../../shared/Text/Text';
import Todo from '../components/Todo';
import {inject, observer} from 'mobx-react';

const GroupedByResponsible = (props) => {
  const store = props.todoStore;
  const modalStore = props.modalStore;
  const {setCurrentTodo, setModalIsOpen} = modalStore;
  const {groupedTodos} = store;
  const openTodo = (todo) => {
    setCurrentTodo(todo);
    setModalIsOpen(true);
  }

  return (
    <div>
      {
        groupedTodos.users.map((user, i) => (
          <div className={s.group} key={i}>
            <Text key={i} className={s.headerRow} text={user.responsible} variant={'font24'}></Text>
            <div className={s.todos}>
              {
                groupedTodos.todos.filter((todo) => todo.responsible.value === user.id)
                  .map((todo, j) => {
                    return <Todo
                      key={`${i},${j}`}
                      header={todo.header}
                      priority={todo.priority.label}
                      dateEnd={todo.dateEnd}
                      responsible={todo.responsible.label}
                      status={todo.status}
                      onClick={() => openTodo(todo)}
                    />
                  })
              }
            </div>
          </div>
        ))
      }
    </div>
  )
};
export default inject('todoStore')(observer(GroupedByResponsible));