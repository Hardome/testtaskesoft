import React, {useEffect} from 'react';
import s from './Modal.module.scss'
import {inject, observer} from 'mobx-react';
import cn from 'classnames';
import Text from '../shared/Text/Text';
import Input from '../shared/Input/Input';
import DatePicker from '../shared/DatePicker/DatePicker';
import Select from '../shared/Select/Select';
import Button from '../shared/Button/Button';
import TextArea from '../shared/TextArea/TextArea';
import {loadingStatus as loadingStatusEnum} from "../../consts";
import {CircularProgress} from "@mui/material";

const Modal = (props) => {
  const modalStore = props.modalStore;
  const {
    modalIsOpen,
    setModalIsOpen,
    currentTodo,
    setCurrentTodoProp,
    setCurrentTodo,
    priorityOptions,
    statusOptions,
    setTodoChanges,
    responsibilitiesOptions,
    userStore,
    loadingStatus
  } = modalStore;
  const active = modalIsOpen ? s.active : '';

  useEffect(() => {
    setModalIsOpen(modalIsOpen);
  }, [setModalIsOpen, modalIsOpen]);

  const setModalClose = () => {
    setModalIsOpen(false);
    setCurrentTodo({});
  }

  return (
    <div className={cn(s.modal, active)} onClick={setModalClose}>
      <div className={s.content} onClick={e => e.stopPropagation()}>
        <Input
          className={s.input}
          placeholder={'Заголовок задачи'}
          disabled={!userStore.isLeader}
          value={currentTodo?.header || ''}
          onChange={(e) => setCurrentTodoProp('header', e.target.value)}
        />
        <TextArea
          disabled={!userStore.isLeader}
          value={currentTodo?.description || ''}
          onChange={(e) => setCurrentTodoProp('description', e.target.value)}
        />
        {loadingStatus === loadingStatusEnum.LOADING && <CircularProgress/>}
        <div className={s.group}>
          <Text text={'Дата окончания:'}/>
          <DatePicker
            disabled={!userStore.isLeader}
            onChange={setCurrentTodoProp}
            value={currentTodo?.dateEnd}
          />
        </div>
        <div className={s.group}>
          <Text text={'Приоритет:'}/>
          <Select
            className={s.select}
            disabled={!userStore.isLeader}
            label={'Приоритет'}
            options={priorityOptions}
            onChange={setCurrentTodoProp}
            value={currentTodo?.priority?.value}
            type={'priority'}
          />
        </div>
        <div className={s.group}>
          <Text text={'Статус:'}/>
          <Select
            className={s.select}
            label={'Статус'}
            options={statusOptions}
            onChange={setCurrentTodoProp}
            value={currentTodo?.status?.value}
            type={'status'}
          />
        </div>
        <div className={s.group}>
          <Text text={'Ответственный:'}/>
          <Select
            className={s.select}
            disabled={!userStore.isLeader}
            label={'Ответственный'}
            options={responsibilitiesOptions}
            onChange={setCurrentTodoProp}
            value={currentTodo?.responsible?.value}
            type={'responsible'}
          />
        </div>
        <Button
          onClick={setTodoChanges}
          text={'Сохранить'}
        />
      </div>
    </div>
  );
}
export default inject('todoStore')(observer(Modal));