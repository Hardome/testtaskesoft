/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  await knex.schema.createTable('users', (table) => {
    table.increments('id');
    table.string('surName').notNullable();
    table.string('firstName').notNullable();
    table.string('secondName').notNullable();
    table.string('login').notNullable();
    table.string('password').notNullable();
    table.integer('idLeader');
    table.foreign('idLeader')
      .references('id')
      .inTable('users');
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  await knex.schema.dropTable('users');
};
