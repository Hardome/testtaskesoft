import React from 'react';
import s from './components.module.scss'
import Text from '../../shared/Text/Text';
import {todoStatus} from '../../../consts';

const Todo = (props) => {
    let status;

    switch (props.status.value) {
      case todoStatus.COMPLETED:
        status = s.completed;
        break;
      case todoStatus.TO_EXECUTE:
      case todoStatus.IN_PROGRESS:
        if (new Date(props.dateEnd).setHours(0, 0, 0, 0) < new Date(Date.now()).setHours(0, 0, 0, 0)) {
          status = s.expired;
        }
        break;
      default:
        break;
    }

    return (
      <div className={s.todo} onClick={props.onClick}>
        <div className={s.header}>
          <Text text={props.header} variant={'font16'} className={status}/>
        </div>
        <div className={s.data}>
          <div className={s.priority}>
            <Text text={'Приоритет:'} className={s.name}/>
            <Text text={props.priority}/>
          </div>
          <div className={s.priority}>
            <Text text={'Дата окончания:'} className={s.name}/>
            <Text text={new Date(props.dateEnd).toLocaleDateString()}/>
          </div>
          <div className={s.priority}>
            <Text text={'Ответственный:'} className={s.name}/>
            <Text text={props.responsible}/>
          </div>
          <div className={s.priority}>
            <Text text={'Статус задачи:'} className={s.name}/>
            <Text text={props.status.label}/>
          </div>
        </div>
      </div>
    )
  }
;
export default Todo;