const groupingType = {
  WITHOUT_GROUPING: 1,
  WITH_GROUPING_BY_DATE: 2,
  WITH_GROUPING_BY_RESPONSIBLE: 3,
}

const loadingStatus = {
  LOADING: 1,
  ERROR: 2,
  SUCCESS: 3
}

const todoStatus = {
  CANCELED: 4,
  COMPLETED: 3,
  IN_PROGRESS: 2,
  TO_EXECUTE: 1
}

const todoPriority = {
  HIGH: 3,
  MEDIUM: 2,
  LOW: 1
}

export {
  groupingType,
  loadingStatus,
  todoPriority,
  todoStatus
}