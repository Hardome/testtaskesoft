import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import {DatePicker} from '@mui/x-date-pickers/DatePicker';
import React from 'react';
import {observer} from 'mobx-react';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';

const DatePickerShared = (props) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale='ru'>
      <DatePicker
        disabled={props.disabled}
        label={props.label}
        onChange={(value) => props.onChange('dateEnd', value)}
        value={dayjs(props.value)}
      />
    </LocalizationProvider>
  )
}
export default (observer(DatePickerShared));