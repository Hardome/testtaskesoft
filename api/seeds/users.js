const {hashSync} = require('bcrypt');
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {

  await knex('todos').del();
  await knex('users').del();

  const startUsers = [
    {
      surName: 'Бедрин',
      firstName: 'Семён',
      secondName: 'Олегович',
      login: 'Bedrin',
      password: hashSync('123456', 7)
    },
    {
      surName: 'Павлов',
      firstName: 'Роман',
      secondName: 'Владимирович',
      login: 'PavlovRoman',
      password: hashSync('Pavlov', 7)
    },
    {
      surName: 'Иванова',
      firstName: 'Ксения',
      secondName: 'Дмитриевна',
      login: 'Ivanova',
      password: hashSync('IvanovaBoss', 7)
    }
  ];

  let _users = await knex('users').insert(startUsers).returning('*');
  console.log('\nInserted users:', _users);

  await knex('users')
    .where('id', _users[0].id)
    .update({idLeader: _users[2].id});
  await knex('users')
    .where('id', _users[1].id)
    .update({idLeader: _users[2].id});

  const randomDate = () => {
    const startDate = new Date(2023, 6, 1);
    const endDate = new Date(2023, 9, 20);
    return new Date(startDate.getTime()
      + Math.random() * (endDate.getTime() - startDate.getTime()));
  }

  const startTodos = [
    {
      header: 'Написать ТЗ',
      description: 'Какое-то подробное описание задачи',
      dateEnd: new Date(Date.now()-1000*60*24*60),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()),
      priority: 1,
      status: 1,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Задача среднего приоритета',
      description: 'Какое-то подробное описание задачи, только подлинее',
      dateEnd: new Date(new Date(Date.now()+1000*60*24*60)),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()),
      priority: 2,
      status: 1,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Задача высокого приоритета',
      description: 'Какое-то подробное описание задачи, только подлинее и еще длинее',
      dateEnd: randomDate(),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()),
      priority: 3,
      status: 1,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Задача выполняется',
      description: 'Задача для Бедрина, которая выполняется',
      dateEnd: randomDate(),
      createdAt: new Date(Date.now()+1000*60*60*4),
      updatedAt: new Date(Date.now()),
      priority: 3,
      status: 2,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Задача не выполнена',
      description: 'Задача для Бедрина, которая не выполняется',
      dateEnd: randomDate(),
      createdAt: new Date(Date.now()+1000*60*60*5),
      updatedAt: new Date(Date.now()),
      priority: 3,
      status: 4,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Задача выполнена',
      description: 'Задача для Бедрина, которая не выполнена',
      dateEnd: randomDate(),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()),
      priority: 1,
      status: 3,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Задача выполняется',
      description: 'Задача для Бедрина, которая не выполнена 2',
      dateEnd: randomDate(),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()),
      priority: 2,
      status: 2,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Задача 1',
      description: 'Задача для 2 юзера, которая что-то там 2',
      dateEnd: randomDate(),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()),
      priority: 2,
      status: 3,
      idAuthor: _users[2].id,
      idResponsible: _users[1].id
    },
    {
      header: 'Задача для руководителя',
      description: 'Задача для руководителя, сама себе назначила',
      dateEnd: randomDate(),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()+100*24),
      priority: 3,
      status: 3,
      idAuthor: _users[2].id,
      idResponsible: _users[2].id
    },
    {
      header: 'Просрочила',
      description: 'Задача руководителя, просроченная',
      dateEnd: new Date(Date.now()-1000*24*60*60*3),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()+222*24),
      priority: 1,
      status: 1,
      idAuthor: _users[2].id,
      idResponsible: _users[2].id
    },
    {
      header: 'Обычная задача',
      description: 'Написать сид для входных данных',
      dateEnd: new Date(Date.now()+100),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()),
      priority: 1,
      status: 1,
      idAuthor: _users[2].id,
      idResponsible: _users[0].id
    },
    {
      header: 'Обычная задача Павлову',
      description: 'Текста хоть сколько могу написать, в БД колона TEXT',
      dateEnd: new Date(Date.now()+1000),
      createdAt: new Date(Date.now()),
      updatedAt: new Date(Date.now()+1000),
      priority: 1,
      status: 2,
      idAuthor: _users[2].id,
      idResponsible: _users[1].id
    },
  ];

  let _todos = await knex('todos').insert(startTodos).returning('*');
  console.log('\nInserted todos:', _todos);
};
