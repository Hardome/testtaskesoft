import React from 'react';
import {MobXProviderContext} from 'mobx-react';

const Provider = ({children, ...stores}) => (
  <MobXProviderContext.Provider value={
    {
      ...React.useContext(MobXProviderContext),
      ...stores
    }
  }
  >
    {children}
  </MobXProviderContext.Provider>
);

export {Provider};