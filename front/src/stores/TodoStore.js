import {makeAutoObservable} from 'mobx';
import {groupingType, todoPriority, todoStatus} from '../consts';

class TodoStore {
  constructor(userStore) {
    makeAutoObservable(this, {}, {autoBind: true});
    this.userStore = userStore;
    this._init();
  }

  todos = [];
  groupedTodos = {};
  selectedGroupingType = groupingType.WITHOUT_GROUPING;
  _init = async () => {
    await this.getTodos();
    this.setSelectedGroupingType(groupingType.WITHOUT_GROUPING);
  }

  setTodos = (todos) => {
    this.todos = todos;
  }

  setSelectedGroupingType = (type) => {
    switch (type) {
      case groupingType.WITHOUT_GROUPING:
        this.sortTodosByDate();
        break;
      case groupingType.WITH_GROUPING_BY_DATE:
        this.todos.sort((a, b) => new Date(a.dateEnd) > new Date(b.dateEnd) ? 1 : -1);
        this.groupByDate();
        break;
      case groupingType.WITH_GROUPING_BY_RESPONSIBLE:
        this.todos.sort((a, b) => new Date(a.dateEnd) > new Date(b.dateEnd) ? 1 : -1);
        this.groupByResponsible();
        break;
      default:
        return null;
    }

    this.selectedGroupingType = type;
  }

  refreshTodos = async () => {
    await this.getTodos();
    this.setSelectedGroupingType(this.selectedGroupingType);
  }
  groupByResponsible = () => {
    const users = this.todos.reduce((acc, todo) => {
      const user = {id: todo.resId, responsible: this.mapUser(todo.resSurName, todo.resFirstName, todo.resSecondName)}
      return acc.some(item => item.id === user.id) ? acc : [...acc, user];
    }, []);

    const groupedTodos = {
      users: users.sort((a, b) => a.id > b.id ? 1 : -1),
      todos: this.todos.map(this.formatTodo),
    }

    this.setGroupedTodos(groupedTodos);
  }

  sortTodosByDate = () => {
    const todos = this.todos.sort((a, b) => new Date(a.updatedAt) > new Date(b.updatedAt) ? -1 : 1);
    this.setGroupedTodos({withoutGrouping: todos.map(this.formatTodo)});
  }

  groupByDate = () => {
    let todayTodos = this.todos.filter((todo) =>
      new Date(todo.dateEnd).setHours(0, 0, 0, 0) === new Date(Date.now()).setHours(0, 0, 0, 0));
    let weekTodos = this.todos.filter((todo) =>
      new Date(todo.dateEnd).setHours(0, 0, 0, 0) > new Date(Date.now()).setHours(0, 0, 0, 0))
      .filter((todo) => new Date(todo.dateEnd).setHours(0, 0, 0, 0) < new Date(Date.now() + 1000 * 60 * 60 * 24 * 7).setHours(0, 0, 0, 0));
    let futureTodos = this.todos.filter((todo) =>
      new Date(todo.dateEnd).setHours(0, 0, 0, 0) > new Date(Date.now() + 1000 * 60 * 60 * 24 * 7).setHours(0, 0, 0, 0));

    const groupedTodos = {
      todayTodos: todayTodos.map(this.formatTodo),
      weekTodos: weekTodos.map(this.formatTodo),
      futureTodos: futureTodos.map(this.formatTodo),
    }

    this.setGroupedTodos(groupedTodos);
  }

  setGroupedTodos = (todos) => {
    this.groupedTodos = todos;
  }

  formatTodo = (todo) => {
    return {
      id: todo.id,
      header: todo.header,
      priority: {value: todo.priority, label: this.mapPriority(todo.priority)},
      dateEnd: todo.dateEnd,
      responsible: {value: todo.resId, label: this.mapUser(todo.resSurName, todo.resFirstName, todo.resSecondName)},
      status: {value: todo.status, label: this.mapStatus(todo.status)},
      description: todo.description
    }
  }

  mapUser = (secondName, firstName, surName) => {
    return `${secondName} ${firstName[0]}. ${surName[0]}.`
  }

  mapStatus = (status) => {
    switch (status) {
      case todoStatus.TO_EXECUTE:
        return 'К выполнению';
      case todoStatus.IN_PROGRESS:
        return 'Выполняется'
      case todoStatus.COMPLETED:
        return 'Выполнена'
      case todoStatus.CANCELED:
        return 'Отменена'
      default:
        return null;
    }
  }

  mapPriority = (priority) => {
    switch (priority) {
      case todoPriority.LOW:
        return 'Низкий'
      case  todoPriority.MEDIUM:
        return 'Средний'
      case todoPriority.HIGH:
        return 'Высокий'
      default:
        return null;
    }
  }

  getTodos = async () => {
    const res = await fetch(`http://localhost:8000/todos?id=${this.userStore.userData.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    });
    const todos = await res.json();

    if (res.ok) {
      this.setTodos(todos);
    }
  }
}

export default TodoStore;