import React from 'react';
import s from './Header.module.scss'
import {inject, observer} from 'mobx-react';
import Text from '../shared/Text/Text';
import {NavLink} from 'react-router-dom';

const Header = (props) => {
  const store = props.userStore;
  const {logout} = store;

  return (
    <div className={s.header}>
      <div className={s.homeLink}>
        <Text variant={'font24'} text={'Todos'} color={'White'} />
      </div>
      {
        store.isLeader &&
      <div className={s.links}>
        <NavLink className={s.link} to={'/todos'}>Задачи</NavLink>
        <NavLink className={s.link} to={'/admin-panel'}>Админка</NavLink>
      </div>
      }
        <div className={s.exitButton}
             onClick={logout}
        >Выйти
        </div>
    </div>
  )
};

export default inject('userStore')(observer(Header));